#include "tst_mainwindow.hpp"
#include <QTest>
#include <QSplitter>


TestMainWindow::TestMainWindow(Alfa::Application& app):
    alfaApp(app)
{
}

void TestMainWindow::initTestCase()
{
}

void TestMainWindow::cleanupTestCase()
{
}

void TestMainWindow::testCase0()
{
    auto parent = qobject_cast<QWidget*>(&alfaApp);
    mainWindow = new MainWindow(parent);

    QObject* centralWidget = mainWindow->findChild<QObject*>("centralWidget");
    QObject* mainToolBar = mainWindow->findChild<QObject*>("mainToolBar");
    QObject* statusBar = mainWindow->findChild<QObject*>("statusBar");

    QVERIFY(centralWidget);
    QVERIFY(mainToolBar);
    QVERIFY(statusBar);
}


