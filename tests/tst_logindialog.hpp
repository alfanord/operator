#ifndef TST_LOGINDIALOG_HPP
#define TST_LOGINDIALOG_HPP

#include "tst_template.hpp"

#include "../src/application.hpp"

#include "../src/dialogs/logindialog.hpp"



class TestLoginDialog :
    public TestTemplate<LoginDialog>
{
public:
    TestLoginDialog(Alfa::Application& app);
    ~TestLoginDialog() {}

private:

    void testCase1();
};



#endif // TST_LOGINDIALOG_HPP
