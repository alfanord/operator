#ifndef TST_TEMPLATE_HPP
#define TST_TEMPLATE_HPP

#include <QObject>
#include <QApplication>
#include <QTest>

#include "../src/application.hpp"


class TestObject: public QObject
{
    Q_OBJECT

public:
    explicit TestObject(QObject* parent = nullptr): QObject(parent) {}
    ~TestObject() {}

private slots:
    virtual void initTestCase() {}
    virtual void cleanupTestCase() {}
    virtual void testCase0() {}

    virtual void testCase1() {}
};


template <class T>
class TestTemplate:
    public TestObject
{
public:
//    TestTemplate(QObject* parent = nullptr): TestObject(parent) {}

    TestTemplate(Alfa::Application& app):
        testApp(app)
    {
//        testApp = app;
//        testApp.setApplicationName("OperatorUI");
//        testApp.setApplicationVersion("0.0.0");
    }

    ~TestTemplate() {}
protected:
    TestObject* testObject;
private:
    Alfa::Application& testApp;

    virtual void initTestCase() override
    {
        auto mainWindow = new QWidget();
        testObject = new TestObject(mainWindow);
    }

    virtual void cleanupTestCase() override
    {
        if (testObject)
            delete testObject;
    }
    virtual void testCase0() override
    {
        qDebug() << testObject;
        QVERIFY(testObject);
    }


    virtual void testCase1() override {}

};

#endif // TST_TEMPLATE_HPP
