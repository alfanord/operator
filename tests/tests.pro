QT += testlib
QT += gui core network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += qt warn_on depend_includepath testcase c++1z

#DEFINES += NZMQT_LIB

QMAKE_CXXFLAGS_WARN_ON = -Wall -Wno-unused-parameter

LIBS += -lprotobuf

TEMPLATE = app
TARGET = test_app

QMAKE_CXXFLAGS += --coverage
QMAKE_LFLAGS += --coverage


SOURCES +=  \
    main.cpp \
    tst_mainwindow.cpp \
    tst_logindialog.cpp \
    ../src/runguard.cpp \
    ../src/mainwindow.cpp \
    ../src/application.cpp \
    ../src/core/service.cpp \
    ../src/core/restclient.cpp \
    ../src/core/settings.cpp \
    ../src/core/peer.cpp \
    ../src/interfaces/restinterface.cpp \
    ../src/widgets/mainwidget.cpp \
    ../src/widgets/sidebarwidget.cpp \
    ../src/widgets/consolewidget.cpp \
    ../src/widgets/workspacewidget.cpp \
    ../src/widgets/searchwidget.cpp \
    ../src/dialogs/logindialog.cpp \
    ../src/models/base.mdl.cpp \
    ../src/models/account.mdl.cpp \
    ../src/classes/base.cl.cpp \
    ../src/classes/account.cl.cpp \
    ../src/classes/message.cl.cpp \
    ../src/protocol/account.pb.cc \
    ../src/protocol/account_props.pb.cc \
    ../src/protocol/auth.pb.cc \
    ../src/protocol/channel.pb.cc \
    ../src/protocol/common.pb.cc \
    ../src/protocol/csm.pb.cc \
    ../src/protocol/event.pb.cc \
    ../src/protocol/event_type.pb.cc \
    ../src/protocol/message.pb.cc \
    ../src/protocol/packet.pb.cc \
    ../src/protocol/person.pb.cc \
    ../src/protocol/surgard.pb.cc \
    ../src/protocol/trouble.pb.cc \
    ../src/protocol/user.pb.cc \
    ../src/protocol/ips.pb.cc \
    ../src/protocol/wialon_unit.pb.cc \
    ../src/core/helper.cpp \
    ../src/core/packet.cpp



HEADERS += \
    tst_mainwindow.hpp \
    tst_logindialog.hpp \
    tst_template.hpp \
    ../src/runguard.hpp \
    ../src/mainwindow.hpp \
    ../src/application.hpp \
    ../src/core/service.hpp \
    ../src/core/restclient.hpp \
    ../src/core/settings.hpp \
    ../src/core/peer.hpp \
    ../src/interfaces/restinterface.hpp \
    ../src/widgets/mainwidget.hpp \
    ../src/widgets/sidebarwidget.hpp \
    ../src/widgets/consolewidget.hpp \
    ../src/widgets/workspacewidget.hpp \
    ../src/widgets/searchwidget.hpp \
    ../src/dialogs/logindialog.hpp \
    ../src/models/account.mdl.hpp \
    ../src/models/base.mdl.hpp \
    ../src/classes/base.cl.hpp \
    ../src/classes/account.cl.hpp \
    ../src/classes/message.cl.hpp \
    ../src/protocol/account.pb.h \
    ../src/protocol/account_props.pb.h \
    ../src/protocol/auth.pb.h \
    ../src/protocol/channel.pb.h \
    ../src/protocol/common.pb.h \
    ../src/protocol/csm.pb.h \
    ../src/protocol/event.pb.h \
    ../src/protocol/event_type.pb.h \
    ../src/protocol/message.pb.h \
    ../src/protocol/packet.pb.h \
    ../src/protocol/person.pb.h \
    ../src/protocol/surgard.pb.h \
    ../src/protocol/trouble.pb.h \
    ../src/protocol/user.pb.h \
    ../src/protocol/ips.pb.h \
    ../src/protocol/wialon_unit.pb.h \
    ../src/core/helper.hpp \
    ../src/core/packet.hpp



FORMS += \
    ../src/mainwindow.ui \
    ../src/dialogs/logindialog.ui

INCLUDEPATH += \
    ../src
