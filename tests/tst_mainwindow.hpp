#ifndef TST_MAINWINDOW_HPP
#define TST_MAINWINDOW_HPP

#include "../src/mainwindow.hpp"
#include "../src/application.hpp"


class TestMainWindow : public QObject
{
    Q_OBJECT

public:
    TestMainWindow(Alfa::Application&);
//    ~TestMainWindow();

private:
    Alfa::Application& alfaApp;
    MainWindow* mainWindow;

private slots:
    void initTestCase();
    void cleanupTestCase();

    void testCase0();
};



#endif // TST_MAINWINDOW_HPP
