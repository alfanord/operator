#include <QApplication>
#include <QCoreApplication>
#include <QTest>
#include <QVector>

#include <iostream>
#include <cstdlib>
#include <cstdio>

#include "tst_mainwindow.hpp"
#include "tst_logindialog.hpp"
#include "tst_template.hpp"

#include "../src/application.hpp"

#include "../src/widgets/mainwidget.hpp"
#include "../src/widgets/sidebarwidget.hpp"
#include "../src/widgets/workspacewidget.hpp"
#include "../src/widgets/consolewidget.hpp"
#include "../src/widgets/searchwidget.hpp"

#include "../src/core/restclient.hpp"
#include "../src/core/service.hpp"
#include "../src/core/settings.hpp"

#include "../src/dialogs/logindialog.hpp"

#include "../src/classes/account.cl.hpp"


template <class AlfaObject>
static TestObject* testFactory(Alfa::Application& app)
{
    return new TestTemplate<AlfaObject>(app);
}


int main(int argc, char** argv)
{
    Alfa::Application testApp(argc, argv);

    QVector<TestObject*> templateTests;

    auto testMainWindow = new TestMainWindow(testApp);
    auto testLoginDialog = new TestLoginDialog(testApp);

    templateTests.push_back(testFactory<Alfa::Account>(testApp));

    templateTests.push_back(testFactory<Alfa::Application>(testApp));
    templateTests.push_back(testFactory<Alfa::RestClient>(testApp));
    templateTests.push_back(testFactory<Alfa::Service>(testApp));
    templateTests.push_back(testFactory<Alfa::Settings>(testApp));

    templateTests.push_back(testFactory<Alfa::MainWidget>(testApp));
    templateTests.push_back(testFactory<Alfa::SideBarWidget>(testApp));
    templateTests.push_back(testFactory<Alfa::WorkSpaceWidget>(testApp));
    templateTests.push_back(testFactory<Alfa::ConsoleWidget>(testApp));
    templateTests.push_back(testFactory<Alfa::SearchWidget>(testApp));


    QTest::qExec(testMainWindow, argc, argv);
    QTest::qExec(testLoginDialog, argc, argv);

    for (auto const& objectTest : templateTests)
        QTest::qExec(objectTest, argc, argv);

    return EXIT_SUCCESS;
}

