#include "restinterface.hpp"

#include <QObject>
#include <QVariantMap>

#include "../application.hpp"

#include <functional>
#include <typeinfo>

namespace Alfa
{


RestInterface::RestInterface():
    restClient(Application::getApp().getRestClient())
{
    successHandler = std::bind(&RestInterface::handleSuccess, this, std::placeholders::_1);
    errorHandler = std::bind(&RestInterface::handleError, this, std::placeholders::_1);
    finishHandler = std::bind(&RestInterface::handleFinish, this);
}

void RestInterface::handleSuccess(const QJsonObject& result)
{
    auto child = dynamic_cast<QObject*>(this);

    qWarning() << "you must override"  << "in" << child;
    qDebug()  << "result:" << result;
}

void RestInterface::handleError(const QJsonObject& error)
{
    auto child = dynamic_cast<QObject*>(this);

    qCritical() << "you must override"  << "in" << child;
    qCritical()  << "error:" << error;
}

void RestInterface::handleFinish()
{
    auto child = dynamic_cast<QObject*>(this);

    qWarning() << "you must override"  << "in" << child;
    qDebug()  << "request finished";
}


void RestInterface::doPing()
{
    restClient.sendRequest("", successHandler, errorHandler);
}

void RestInterface::doLogin(const QString& login, const QString& password)
{
    QVariantMap data;

    QVariant userName(login);
    QVariant userPassword(password);

    data.insert("login", userName);
    data.insert("password", userPassword);

    restClient.sendRequest("auth/login", successHandler, errorHandler, RestClient::Type::POST, data);
}

void RestInterface::doRefresh(const QString& authToken)
{
    restClient.setToken(authToken);
    restClient.sendRequest("auth/refresh", successHandler, errorHandler);
}

void RestInterface::loadAccounts(const handleFunc& customHandler)
{
    restClient.sendRequest("account", customHandler, errorHandler);
}

void RestInterface::loadAccounts()
{
    restClient.sendRequest("account", successHandler, errorHandler);
}


void RestInterface::setToken(const QString& token)
{
    restClient.setToken(token);
}


} // namespace Alfa
