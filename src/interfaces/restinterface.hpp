#ifndef RESTINTERFACE_HPP
#define RESTINTERFACE_HPP

#include "../core/restclient.hpp"
#include <QObject>

namespace Alfa
{

class RestInterface
{

public:
    using handleFunc = RestClient::handleFunc;
    using finishFunc = RestClient::finishFunc;

    RestInterface();
    virtual ~RestInterface() {}

    void setToken(const QString& token);

private:
    RestClient& restClient;

    handleFunc successHandler;
    handleFunc errorHandler;
    finishFunc finishHandler;

    void virtual handleSuccess(const QJsonObject&);
    void virtual handleError(const QJsonObject&);
    void virtual handleFinish();

protected:
    void loadAccounts(const handleFunc&);
    void loadAccounts();

    void doPing();

    void doLogin(const QString& login, const QString& password);
    void doRefresh(const QString& token);
};


} // namespace Alfa

Q_DECLARE_INTERFACE(Alfa::RestInterface, "alfa.interface.rest/0.0")


#endif // RESTINTERFACE_HPP
