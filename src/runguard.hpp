#ifndef RUNGUARD_HPP
#define RUNGUARD_HPP

#include <QObject>
#include <QSharedMemory>
#include <QSystemSemaphore>

namespace Alfa
{

class RunGuard
{
public:
    RunGuard(const QString& key);
    ~RunGuard();

    bool isAnotherRunning();
    bool tryToRun();
    void release();

private:
    const QString key;
    const QString memLockKey;
    const QString sharedmemKey;

    static QString generateKeyHash(const QString& key, const QString& salt);

    QSharedMemory sharedMem;
    QSystemSemaphore memLock;

    Q_DISABLE_COPY(RunGuard)
};

} // namespace Alfa

#endif // RUNGUARD_HPP
