#include "mainwindow.hpp"

#include <iostream>

#include <QTime>
#include <QDebug>
#include <QString>

#include "application.hpp"
#include "runguard.hpp"

#include "dialogs/logindialog.hpp"



static void alfaLogHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    QByteArray localTime = QDateTime::currentDateTime().toString(Qt::ISODate).toLocal8Bit();

    auto formatFunc = [](const char* rawFuncName)
    {
        QString funcName(rawFuncName);

        int startIdx = funcName.indexOf("(");
        int endIdx = funcName.lastIndexOf(")");

        if ((endIdx - startIdx) > 1)
        {
            funcName.resize(startIdx + 1);
            funcName.append(")");
        }

        return funcName.toLocal8Bit().constData();
    };

    switch (type)
    {
    case QtDebugMsg:
        fprintf(stderr, "[%s DEBUG: %s:%u] %s\n", localTime.constData(), formatFunc(context.function), context.line, localMsg.constData());
        break;

    case QtInfoMsg:
        fprintf(stderr, "[%s  INFO: %s:%u] %s\n", localTime.constData(), formatFunc(context.function), context.line, localMsg.constData());
        break;

    case QtWarningMsg:
        fprintf(stderr, "[%s  WARN: %s:%u] %s\n", localTime.constData(), formatFunc(context.function), context.line, localMsg.constData());
        break;

    case QtCriticalMsg:
        fprintf(stderr, "[%s ERROR: %s:%u] %s\n", localTime.constData(), formatFunc(context.function), context.line, localMsg.constData());
        break;

    case QtFatalMsg:
        fprintf(stderr, "[%s FATAL: %s:%u] %s\n", localTime.constData(), formatFunc(context.function), context.line, localMsg.constData());
        abort();
    }
}



int main(int argc, char** argv)
{

    qInstallMessageHandler(alfaLogHandler);

    Alfa::RunGuard guard("alfanord");

    if (!guard.tryToRun())
    {
        qCritical() << "already running";
        return EXIT_FAILURE;
    }

    Alfa::Application app(argc, argv);

    Alfa::Settings& settings = app.getSettings();
    Alfa::RestClient& restClient = app.getRestClient();
    Alfa::RpcClient& rpcClient = app.getRpcClient();
    Alfa::RpcChannel& rpcChannel = app.getRpcChannel();

    MainWindow mainWindow;
    LoginDialog loginDialog;

//    app.connect(&app, &Alfa::Application::started, &loginDialog, &LoginDialog::showUI);
    app.connect(&app, &Alfa::Application::started, &rpcChannel, &Alfa::RpcChannel::start);
    app.connect(&rpcChannel, &Alfa::RpcChannel::connected, &loginDialog, &LoginDialog::checkAuth);

    app.connect(&app, &Alfa::Application::aboutToQuit, &rpcClient, &Alfa::RpcClient::cleanUp);

    app.connect(&loginDialog, &LoginDialog::authAccepted, &restClient, &Alfa::RestClient::setToken);
    app.connect(&loginDialog, &LoginDialog::authAccepted, &settings, &Alfa::Settings::saveToken);
    app.connect(&loginDialog, &LoginDialog::authAccepted, &app, &Alfa::Application::initAccounts);

    app.connect(&loginDialog, &LoginDialog::authAccepted, &mainWindow, &MainWindow::setupGeometry);
    app.connect(&loginDialog, &LoginDialog::authAccepted, &mainWindow, &MainWindow::setupUI);
    app.connect(&loginDialog, &LoginDialog::authAccepted, &mainWindow, &MainWindow::show);

    app.connect(&loginDialog, &LoginDialog::authRejected, &loginDialog, &LoginDialog::show);

    return app.exec();
}
