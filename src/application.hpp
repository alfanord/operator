#ifndef ALFA_APPLICATION_HPP
#define ALFA_APPLICATION_HPP

#include <QApplication>

#include "core/settings.hpp"
#include "core/restclient.hpp"
#include "core/rpc_client.hpp"
#include "core/rpc_channel.hpp"

#include "classes/user.cl.hpp"
#include "classes/account.cl.hpp"
#include "classes/message.cl.hpp"

#include "events/on_account.hpp"

namespace Alfa
{

class Application :
    public QApplication
{
    Q_OBJECT

public:
    using ptrUser = QSharedPointer<User>;
    using ptrAccount = QSharedPointer<Account>;

    explicit Application(int& argc, char** argv);
    virtual ~Application();

    static Application& getApp();

    RestClient& getRestClient() const;
    RpcClient& getRpcClient() const;
    Settings& getSettings() const;
    RpcChannel& getRpcChannel() const;

    ptrUser getUser() const;

    void setUser(ptrUser user_);

    void customEvent(QEvent* event)
    {
        if (event->type() == ON_ACCOUNT_EVENT)
            qDebug() << "SIDEBARWIDGET";

    }

    void addEventHandler(QEvent::Type t, QObject* obj)
    {
        if (eventHandlers.contains(t))
            eventHandlers[t].push_back(obj);
        else
            eventHandlers.insert(t, QVector<QObject*>({obj}));
    }
signals:
    void started();

private:
    Settings* settings;
    RestClient* restClient;
    RpcClient* rpcClient;
    RpcChannel* rpcChannel;

    ptrUser user;

    QMap<int, ptrAccount> accounts;
    QMap<int, ptrUser> users;

    QMap<QEvent::Type, QVector<QObject*>> eventHandlers;

private slots:
    void start();

public slots:
    void initAccounts();
};

} // namespace Alfa


#endif // ALFA_APPLICATION_HPP
