#ifndef RPC_RESULT_HPP
#define RPC_RESULT_HPP

//#include "protocol/rpc.pb.h"

namespace Alfa
{

class RpcResult
{
public:
    RpcResult();

    bool hasError() const;

private:
    int error = -1;

};

} // namespace Alfa

#endif // RPC_RESULT_HPP
