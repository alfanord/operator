#include "rpc_call.hpp"

#include <QObject>
#include <sstream>
#include "helper.hpp"

namespace Alfa
{



RpcCall::RpcCall(gpb::Message* req, gpb::Message* res, const RpcCall::handleFunc& handler)
{
    request = QSharedPointer<gpb::Message>(req);
    response = QSharedPointer<gpb::Message>(res);
    handlerFunc = handler;
}

} // namespace Alfa
