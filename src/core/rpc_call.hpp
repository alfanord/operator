#ifndef PACKET_HPP
#define PACKET_HPP

#include <QString>
#include <QObject>
#include <QDebug>
#include "protocol/packet.pb.h"
//#include "protocol/rpc.pb.h"

#include "rpc_result.hpp"

namespace gpb = google::protobuf;

namespace Alfa
{


class RpcCall:
    public QObject
{
    Q_OBJECT
public:

    using ptrMessage = QSharedPointer<gpb::Message>;
    using handleFunc = std::function<void(gpb::Message*)>;

    RpcCall(gpb::Message* req, gpb::Message* res, const handleFunc& handler);

    virtual ~RpcCall()
    {
        qDebug() << *this;
    }


    friend QDebug operator<< (QDebug output, const RpcCall& pkt)
    {
        return (output << pkt.request->ShortDebugString().c_str());
    }


    void handleResponse(bool stream)
    {
        qDebug() << response->ShortDebugString().c_str();
        handlerFunc(response.data());

        if (!stream)
            emit completed();
    }


    gpb::Message* getResponse()
    {
        return response.data();
    }

private:
    ptrMessage request;
    ptrMessage response;

    handleFunc handlerFunc;

signals:
    void completed();
};


} // namespace Alfa

#endif // PACKET_HPP
