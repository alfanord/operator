#ifndef PEER_HPP
#define PEER_HPP

#include <QObject>
#include <QDataStream>
#include <QTcpSocket>
#include <QMap>
#include <QNetworkSession>

#include <google/protobuf/service.h>

#include "core/rpc_call.hpp"
#include "settings.hpp"

#include "protocol/message.pb.h"
#include "protocol/surgard.pb.h"
#include "protocol/auth.pb.h"
#include "protocol/common.pb.h"
#include "protocol/account.pb.h"
#include "protocol/packet_rpc.pb.h"

namespace Alfa
{

class RpcChannel :
    public QObject,
    public google::protobuf::RpcChannel
{
    Q_OBJECT
public:
    using ptrCall = QSharedPointer<RpcCall>;

    explicit RpcChannel(Settings& settings, QObject* parent = nullptr);

    virtual ~RpcChannel() {}

    virtual void CallMethod(const google::protobuf::MethodDescriptor* method,
                            google::protobuf::RpcController* controller,
                            const google::protobuf::Message* request,
                            google::protobuf::Message* response,
                            google::protobuf::Closure* done) override;

private:
    QTcpSocket tcpSocket;
    QDataStream inStream;
    QByteArray readBuffer;

    int callIdx = 0;

    QMap<int, ptrCall> pendingRpcCalls;

    const int MAX_READ_BUF_SIZE = 4096;

    QString peerHost;
    int peerPort;

    bool packBabe(const Proto::RpcPacket& pkt, QByteArray& array);
    bool packBabe(const Proto::RpcPacket& pkt, std::string&);

    bool packFace(const Proto::Packet& pkt, QByteArray& array);
    bool packFace(const Proto::Packet& pkt, std::string&);

    void processBabe(QByteArray&);
    void processFace(QByteArray&);

signals:
    void authAccepted(const QString&);
    void authRejected(const QString&);

    void connected();
    void disconnected();

private slots:
    void handleRead();
    void handleError(const QAbstractSocket::SocketError& socketError);

    void onConnected();
    void onDisconnected();

public slots:
    void start();
};

} // namespace Alfa

#endif // PEER_HPP
