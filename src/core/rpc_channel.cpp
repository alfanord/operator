#include "rpc_channel.hpp"

//#include <QNetworkConfigurationManager>
#include <QIODevice>
#include <QDataStream>

#include <sstream>

#include "core/rpc_call.hpp"
//#include "protocol/rpc.pb.h"
//#include "protocol/auth.pb.h"

#include "helper.hpp"


namespace Alfa
{

RpcChannel::RpcChannel(Settings& settings, QObject* parent) :
    QObject(parent),
    tcpSocket(this)
{
    inStream.setDevice(&tcpSocket);

    peerHost = settings.read("Peer", "host", "localhost").toString();
    peerPort = settings.read("Peer", "port", 7721).toInt();

    connect(&tcpSocket, &QTcpSocket::connected, this, &RpcChannel::onConnected);
    connect(&tcpSocket, &QTcpSocket::disconnected, this, &RpcChannel::onDisconnected);

    connect(&tcpSocket, &QIODevice::readyRead, this, &RpcChannel::handleRead);
    connect(&tcpSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &RpcChannel::handleError);
}


void RpcChannel::CallMethod(const google::protobuf::MethodDescriptor* method,
                            google::protobuf::RpcController* controller,
                            const google::protobuf::Message* request,
                            google::protobuf::Message* response,
                            google::protobuf::Closure* done)
{
    Proto::RpcPacket pkt;
    std::string method_name = method->name();

    pkt.set_idx(++callIdx);
    pkt.set_method(method_name);

    auto rpcController  = dynamic_cast<Alfa::RpcController*>(controller);
    auto rpcCall = rpcController->getRpcCall();

    pkt.mutable_body()->PackFrom(*request);

    qDebug() << pkt.ShortDebugString().c_str();

    QByteArray buffer;

    if (packBabe(pkt, buffer))
    {
        tcpSocket.write(buffer);

        if (!pendingRpcCalls.contains(callIdx))
            pendingRpcCalls.insert(callIdx, rpcCall);
        else
            qWarning() << "rpc call overwrite" << *rpcCall;
    }
    else
        qCritical() << *rpcCall;

    rpcController->Reset();
}

void RpcChannel::handleRead()
{
    inStream.startTransaction();

    QByteArray temp;
    quint32 magic;

    inStream >> magic;

    if (magic != 0xDEADFACE && magic != 0xABADBABE)
    {
        inStream.commitTransaction();
        qCritical() << "bad magic:" << magic;
        return;
    }

    inStream >> temp;

    if (!inStream.commitTransaction())
        return;

    switch (magic)
    {
    case 0xABADBABE:
        processBabe(temp);
        break;

    case 0xDEADFACE:
        processFace(temp);
        break;

    default:
        qWarning() << "warning";
        break;
    }
}

void RpcChannel::handleError(const QAbstractSocket::SocketError& socketError)
{
    qCritical() << socketError;
}

void RpcChannel::onConnected()
{
    qInfo() << "connected to" << tcpSocket.peerAddress();

    emit connected();
}

void RpcChannel::onDisconnected()
{
    qInfo() << "disconnected from" << tcpSocket.peerAddress();

    emit disconnected();
}


bool RpcChannel::packBabe(const Proto::RpcPacket& pkt, std::string& output)
{
    std::ostringstream os;

    os << Helper::FourBytes(0xABADBABE)
       << Helper::FourBytes(pkt.ByteSize());

    bool res = pkt.SerializeToOstream(&os);
    output = os.str();

    return res;
}


bool RpcChannel::packBabe(const Proto::RpcPacket& pkt, QByteArray& array)
{
    std::string temp;
    bool res = packBabe(pkt, temp);
    array.append(temp.c_str(), temp.size());

    return res;
}

bool RpcChannel::packFace(const Proto::Packet& pkt, std::string& output)
{
    std::ostringstream os;

    os << Helper::FourBytes(0xDEADFACE)
       << Helper::FourBytes(pkt.ByteSize());

    bool res = pkt.SerializeToOstream(&os);
    output = os.str();

    return res;
}

void RpcChannel::processBabe(QByteArray& temp)
{
    Proto::RpcPacket rpcPkt;

    if (rpcPkt.ParseFromArray(temp.data(), temp.size()))
    {
        if (pendingRpcCalls.contains(rpcPkt.idx()))
        {
            auto call = pendingRpcCalls.value(rpcPkt.idx());

            if (call)
            {
                gpb::Message* msg = call->getResponse();
                rpcPkt.body().UnpackTo(msg);
                call->handleResponse(rpcPkt.stream());

                if (!rpcPkt.stream())
                    pendingRpcCalls.remove(rpcPkt.idx());
            }
        }

        else
            qCritical() << "no such Rpc Call: " << rpcPkt.ShortDebugString().c_str();
    }
    else
        qCritical() << "bad rpc packet" << temp.constData();
}

void RpcChannel::processFace(QByteArray& temp)
{
    Proto::Packet pkt;

    if (pkt.ParseFromArray(temp.data(), temp.size()))
        qDebug() << pkt.ShortDebugString().c_str();
    else
        qCritical() << temp.constData();
}

bool RpcChannel::packFace(const Proto::Packet& pkt, QByteArray& array)
{
    std::string temp;
    bool res = packFace(pkt, temp);
    array.append(temp.c_str(), temp.size());

    return res;
}

void RpcChannel::start()
{
    tcpSocket.connectToHost(peerHost, peerPort);

}

} // namespace Alfa
