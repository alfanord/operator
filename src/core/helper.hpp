#ifndef HELPER_HPP
#define HELPER_HPP

#include <string>
#include <iostream>

#include "application.hpp"

#define alfaApp() (Alfa::Application::getApp())

constexpr unsigned int _hash(const char* str, int h = 0)
{
    return !str[h] ? 5381 : (_hash(str, h + 1) * 33) ^ str[h];
}

constexpr auto operator  "" _(char const* p, size_t)
{
    return _hash(p);
}


namespace Helper
{


class FourBytes
{

public:
    FourBytes(unsigned long n);
    FourBytes(const std::string& temp);

    unsigned long to_int() const;
    std::string to_string();

    friend std::ostream& operator<< (std::ostream& stream, const FourBytes& fb)
    {
        return (stream << std::string(fb.bytes_, 4));
    }

private:
    char bytes_[4];
};




} // namespace Helper

#endif // HELPER_HPP
