#include "restclient.hpp"

namespace Alfa
{

const QString RestClient::httpTemplate = "http://%1:%2/%3";
const QString RestClient::httpsTemplate = "https://%1:%2/%3";
const QString RestClient::KEY_QNETWORK_REPLY_ERROR = "QNetworkReplyError";
const QString RestClient::KEY_CONTENT_NOT_FOUND = "ContentNotFoundError";

RestClient::RestClient(Settings& settings, QObject* parent):
    QObject(parent)
{
    apiHost = settings.read("Api", "host", "localhost").toString();
    apiPort = settings.read("Api", "port", 5000).toInt();

    bool hasSSL = settings.read("Api", "ssl", false).toBool();

    if (hasSSL)
    {
        sslConfig = QSslConfiguration::defaultConfiguration();
        pathTemplate = httpsTemplate;
    }
    else
        pathTemplate = httpTemplate;
}


void RestClient::sendRequest(const QString& apiStr,
                             const handleFunc& funcSuccess,
                             const handleFunc& funcError,
                             RestClient::Type type,
                             const QVariantMap& data)
{
    QNetworkRequest request = createRequest(apiStr);

    QNetworkReply* reply;

    switch (type)
    {
    case Type::POST:
    {
        QByteArray postData = variantMapToJson(data);
        reply = manager.post(request, postData);
        break;
    }

    case Type::GET:
    {
        reply = manager.get(request);
        break;
    }

    case Type::DELETE:
    {
        if (data.isEmpty())
            reply = manager.deleteResource(request);
        else
            reply = sendCustomRequest(request, "DELETE", data);

        break;
    }

    case Type::PATCH:
    {
        reply = sendCustomRequest(request, "PATCH", data);
        break;
    }

    default:
        reply = nullptr;
        Q_ASSERT(false);
    }

    connect(reply, &QNetworkReply::finished, this, [&, funcSuccess, funcError, reply]()
    {
        QJsonObject obj = parseReply(reply);

        if (onFinishRequest(reply))
        {
            if (funcSuccess != nullptr)
                funcSuccess(obj);
        }
        else
        {
            if (funcError != nullptr)
            {
                handleQtNetworkErrors(reply, obj);
                funcError(obj);
            }
        }

        reply->close();
        reply->deleteLater();
    });

}

void RestClient::sendMulishGetRequest(const QString& apiStr, //а ничего что здесь нигде не проверяется func != nullptr?
                                      const handleFunc& funcSuccess,
                                      const handleFunc& funcError,
                                      const finishFunc& funcFinish)
{
    QNetworkRequest request = createRequest(apiStr);
    //    QNetworkReply *reply;
    qInfo() << "GET REQUEST " << request.url().toString() << "\n";

    auto reply = manager.get(request);

    connect(reply, &QNetworkReply::finished, this, [this, funcSuccess, funcError, funcFinish, reply]()
    {
        QJsonObject obj = parseReply(reply);

        if (onFinishRequest(reply))
        {
            if (funcSuccess != nullptr)
                funcSuccess(obj);

            QString nextPage = obj.value("next").toString();

            if (!nextPage.isEmpty())
            {
                QStringList apiMethodWithPage = nextPage.split("api/");
                sendMulishGetRequest(apiMethodWithPage.value(1),
                                     funcSuccess,
                                     funcError,
                                     funcFinish
                                    );
            }
            else
            {
                if (funcFinish != nullptr)
                    funcFinish();
            }
        }
        else
        {
            handleQtNetworkErrors(reply, obj);

            if (funcError != nullptr)
                funcError(obj);
        }

        reply->close();
        reply->deleteLater();
    });
}


QString RestClient::getToken() const
{
    return authToken;
}

void RestClient::setToken(const QString& value)
{
    authToken = value;
}




QByteArray RestClient::variantMapToJson(QVariantMap data)
{

    QJsonDocument postDataDoc = QJsonDocument::fromVariant(data);

    QByteArray postDataByteArray = postDataDoc.toJson();

    return postDataByteArray;
}

QNetworkRequest RestClient::createRequest(const QString& apiStr)
{
    QNetworkRequest request;
    QString url = pathTemplate.arg(apiHost).arg(apiPort).arg(apiStr);
    request.setUrl(QUrl(url));
    request.setRawHeader("Content-Type", "application/json");

    if (!authToken.isEmpty())
        request.setRawHeader("Authorization", QString("Bearer %1").arg(authToken).toUtf8());

    if (!sslConfig.isNull())
        request.setSslConfiguration(sslConfig);

    return request;
}

QNetworkReply* RestClient::sendCustomRequest(
    QNetworkRequest& request,
    const QString& type,
    const QVariantMap& data)
{
    request.setRawHeader("HTTP", type.toUtf8());
    QByteArray postDataByteArray = variantMapToJson(data);
    QBuffer* buff = new QBuffer;
    buff->setData(postDataByteArray);
    buff->open(QIODevice::ReadOnly);
    QNetworkReply* reply =  manager.sendCustomRequest(request, type.toUtf8(), buff);
    buff->setParent(reply);
    return reply;
}

QJsonObject RestClient::parseReply(QNetworkReply* reply)
{
    QJsonObject jsonObj;
    QJsonDocument jsonDoc;
    QJsonParseError parseError;
    auto replyText = reply->readAll();
    jsonDoc = QJsonDocument::fromJson(replyText, &parseError);

    if (parseError.error != QJsonParseError::NoError)
    {
        qDebug() << replyText;
        qWarning() << "Json parse error: " << parseError.errorString();
    }
    else
    {
        if (jsonDoc.isObject())
            jsonObj  = jsonDoc.object();
        else if (jsonDoc.isArray())
            jsonObj["non_field_errors"] = jsonDoc.array();
    }

    return jsonObj;
}

bool RestClient::onFinishRequest(QNetworkReply* reply)
{
    auto replyError = reply->error();

    if (replyError == QNetworkReply::NoError)
    {
        int code = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

        if ((code >= 200) && (code < 300))
            return true;
    }

    return false;
}

void RestClient::handleQtNetworkErrors(QNetworkReply* reply, QJsonObject& obj)
{
    auto replyError = reply->error();

    if (!(replyError == QNetworkReply::NoError ||
          replyError == QNetworkReply::ContentNotFoundError ||
          replyError == QNetworkReply::ContentAccessDenied ||
          replyError == QNetworkReply::ProtocolInvalidOperationError
         ))
    {
        qDebug() << reply->error();
        obj[KEY_QNETWORK_REPLY_ERROR] = reply->errorString();
    }
    else if (replyError == QNetworkReply::ContentNotFoundError)
        obj[KEY_CONTENT_NOT_FOUND] = reply->errorString();
}


} // namespace Alfa

