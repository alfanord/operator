#ifndef SERVICE_HPP
#define SERVICE_HPP

#include <QObject>
#include <QVector>
#include <QSharedPointer>

#include "functional"

#include  "protocol/service_alfa.pb.h"

#include "rpc_call.hpp"
#include "vector"
#include "memory"

#include "rpc_controller.hpp"
#include "rpc_result.hpp"

#include "../interfaces/restinterface.hpp"
#include "../models/account.mdl.hpp"

namespace gpb = google::protobuf;

namespace Alfa
{


class RpcClient :
    public QObject
{
    Q_OBJECT

public:
    using ptrAccountModel = QSharedPointer<AccountModel>;
    using ptrCall = QSharedPointer<RpcCall>;
    using ptrMessage = QSharedPointer<gpb::Message>;

    using handleFunc = std::function<void(gpb::Message*)>;

    using finishFunc = std::function<void()>;

    explicit RpcClient(google::protobuf::RpcChannel*, QObject* = 0);
    ~RpcClient();

    virtual void setupUI() {}

    AccountModel* getAccountModel() const;

    template <typename Req, typename Res>
    void createRpcCall(Req* req, Res* res, const handleFunc& cb)
    {
        gpb::Message* reqPtr = dynamic_cast<gpb::Message*>(req);
        gpb::Message* resPtr = dynamic_cast<gpb::Message*>(res);
        ptrCall call = QSharedPointer<RpcCall>(new RpcCall(reqPtr, resPtr, cb));

        connect(call.data(), &RpcCall::completed, this, &RpcClient::onCallCompeted);

        rpcController.setRpcCall(call);
    }

    void authLogin(const QString&, const QString&, const handleFunc& handler);
    void authRefresh(const QString&, const handleFunc& handler);
    void getAccounts(const handleFunc& handler);
    void getAccountById(int, const handleFunc& handler);

private:
    void handleSuccess(const QJsonObject& json);

    Proto::AlfaRpc_Stub rpcStub;
    RpcController rpcController;

    ptrAccountModel accountModel;

protected:

signals:

public slots:
    void cleanUp();

    void onCallCompeted();

};

} // namespace Alfa




#endif // SERVICE_HPP
