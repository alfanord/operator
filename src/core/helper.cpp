#include "helper.hpp"

#include <QDebug>

namespace Helper
{
FourBytes::FourBytes(unsigned long n)
{
    bytes_[0] = (n >> 24) & 0xFF;
    bytes_[1] = (n >> 16) & 0xFF;
    bytes_[2] = (n >> 8) & 0xFF;
    bytes_[3] = n & 0xFF;
}

FourBytes::FourBytes(const std::string& temp)
{
    if (temp.size() == 4)
    {
        std::size_t length = temp.copy(bytes_, 4, 0);

        if (length < 4)
            bytes_[length] = 0;
    }
    else
        qCritical() << "wrong size";

}

unsigned long FourBytes::to_int() const
{
    return static_cast<uint32_t>((unsigned char)bytes_[0] << 24 |
                                 (unsigned char)bytes_[1] << 16 |
                                 (unsigned char)bytes_[2] << 8 |
                                 (unsigned char)bytes_[3]);
}

std::string FourBytes::to_string()
{
    return std::string(bytes_, 4);
}


} // namespace Helper
