#ifndef REQUESTER_H
#define REQUESTER_H

#include <QObject>
#include <QBuffer>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <functional>

#include "settings.hpp"

namespace Alfa
{

class RestClient :
    public QObject
{
    Q_OBJECT

public:
    using handleFunc = std::function<void(const QJsonObject&)>;
    using finishFunc = std::function<void()>;

    static const QString KEY_QNETWORK_REPLY_ERROR;
    static const QString KEY_CONTENT_NOT_FOUND;

    enum class Type
    {
        POST,
        GET,
        PATCH,
        DELETE // TODO: check windows build
    };

    explicit RestClient(Settings& settings, QObject* = 0);

    void sendRequest(const QString& apiStr,
                     const handleFunc& funcSuccess,
                     const handleFunc& funcError,
                     Type type = Type::GET,
                     const QVariantMap& data = QVariantMap());


    void sendMulishGetRequest(const QString& apiStr,
                              const handleFunc& funcSuccess,
                              const handleFunc& funcError,
                              const finishFunc& funcFinish);

    QString getToken() const;
    void setToken(const QString& value);

private:
    QNetworkAccessManager manager;
    QSslConfiguration sslConfig;

    static const QString httpTemplate;
    static const QString httpsTemplate;

    QString apiHost;
    int apiPort;

    QString authToken;

    QString pathTemplate;

    QByteArray variantMapToJson(QVariantMap data);

    QNetworkRequest createRequest(const QString& apiStr);

    QNetworkReply* sendCustomRequest(QNetworkRequest& request,
                                     const QString& type,
                                     const QVariantMap& data);

    QJsonObject parseReply(QNetworkReply* reply);

    bool onFinishRequest(QNetworkReply* reply);

    void handleQtNetworkErrors(QNetworkReply* reply, QJsonObject& obj);


signals:
    void networkError();

public slots:
};

} // namespace Alfa

#endif // REQUESTER_H
