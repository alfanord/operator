#include "settings.hpp"

namespace Alfa
{

Settings::~Settings() {}

void Settings::saveToken(const QString& authToken)
{
    save("General", "auth_token", authToken);
}

} // namespace Alfa


