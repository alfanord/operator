#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <QSettings>

#include <QObject>

namespace Alfa
{

class Settings :
    public QSettings
{
    Q_OBJECT
public:
    using QSettings::QSettings;
    ~Settings();

    template <class V>
    void save(const QString&  section, const QString& key, const V& value)
    {
        beginGroup(section);
        setValue(key, value);
        endGroup();
    }

    template <class V>
    QVariant read(const QString&  section, const QString& key, const V& value)
    {
        beginGroup(section);
        auto result = this->value(key, value);
        endGroup();

        return result;
    }

private:

public slots:
    void saveToken(const QString& token);

};

} // namespace Alfa

#endif // SETTINGS_HPP

