#include "rpc_client.hpp"

#include "../classes/account.cl.hpp"

//#include "protocol/rpc.pb.h"

namespace Alfa
{

RpcClient::RpcClient(google::protobuf::RpcChannel* rpc_channel, QObject* parent):
    QObject(parent),
    rpcStub(rpc_channel)
{
    accountModel = ptrAccountModel::create(this);
}

RpcClient::~RpcClient() {}


AccountModel* RpcClient::getAccountModel() const
{
    return accountModel.data();
}

void RpcClient::handleSuccess(const QJsonObject& json)
{
    qDebug()  << json;
}

void RpcClient::authLogin(const QString& username, const QString& password, const handleFunc& handler)
{
    Proto::AuthLoginReq* req = new Proto::AuthLoginReq();
    Proto::AuthLoginRes* res = new Proto::AuthLoginRes();

    req->set_username(username.toLocal8Bit());
    req->set_password(password.toLocal8Bit());

    createRpcCall(req, res, handler);

    rpcStub.AuthLogin(&rpcController, req, res, nullptr);
}



void RpcClient::authRefresh(const QString& userToken, const RpcClient::handleFunc& handler)
{
    Proto::AuthRefreshReq* req = new Proto::AuthRefreshReq();
    Proto::AuthRefreshRes* res = new Proto::AuthRefreshRes();

    req->set_user_token(userToken.toLocal8Bit());

    createRpcCall(req, res, handler);

    rpcStub.AuthRefresh(&rpcController, req, res, nullptr);
}

void RpcClient::getAccounts(const handleFunc& handler)
{

    Proto::GetAccountsReq* req = new Proto::GetAccountsReq();
    Proto::GetAccountsRes* res = new Proto::GetAccountsRes();

//    if (!ids.empty())
//    {
//        for (int i = 0; i < ids.size(); ++i)
//            req->add_ids(ids.at(i));
//    }

    createRpcCall(req, res, handler);

    rpcStub.GetAccounts(&rpcController, req, res, nullptr);
}

void RpcClient::getAccountById(int id, const handleFunc& handler)
{
    Proto::GetAccountByIdReq* req = new Proto::GetAccountByIdReq();
    Proto::GetAccountByIdRes* res = new Proto::GetAccountByIdRes();

    req->set_id(id);

    createRpcCall(req, res, handler);

    rpcStub.GetAccountById(&rpcController, req, res, nullptr);
}

/*
{
    loadAccounts([&](const QJsonObject & json)
    {
        auto jsonData = json["data"].toArray();

        if (!jsonData.empty())
        {
            QMap<int, Account*> accountMap;

            for (auto const& v : jsonData)
            {
                Account* account = new Account(this);

                auto accountJson = v.toObject();
                account->init(accountJson);

                int id = account->getId();
                accountMap[id] = account;
            }

            bool hasValues = !accountMap.isEmpty();

            if (hasValues)
            {
                accountModel->init(accountMap);
                qInfo()  << "loaded" << accountModel->rowCount() << "accounts";
            }
            else
                qWarning()  << jsonData;
        }
    });
}
*/

void RpcClient::cleanUp()
{
    qDebug()  << "some cleanup code";
}

void RpcClient::onCallCompeted()
{
    qDebug()  << "rpc call completed";
}


} // namespace Alfa

