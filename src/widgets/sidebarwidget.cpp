#include "sidebarwidget.hpp"
#include <QTextEdit>
#include <QDebug>

#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>


namespace Alfa
{

SideBarWidget::SideBarWidget(QWidget* parent) :
    QWidget(parent)
{
    accountList = new QListView(this);
    rootLayout = new QVBoxLayout(this);

    searchWidget = new SearchWidget(this);


}

SideBarWidget::~SideBarWidget() {}


void SideBarWidget::handleSuccess(const QJsonObject& result)
{
    qDebug() << this << result;
}


void SideBarWidget::handleError(const QJsonObject& result)
{
    qDebug() << this << result;
}


void SideBarWidget::setupUI()
{
    searchWidget->setupUI();

    rootLayout->setMargin(1);
    rootLayout->addWidget(searchWidget);
    rootLayout->addWidget(accountList);

    setLayout(rootLayout);

    connect(searchWidget, &SearchWidget::modelChanged, accountList, &QListView::setModel);
}


} // namespace Alfa

