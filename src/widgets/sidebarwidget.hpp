#ifndef SIDEBARWIDGET_HPP
#define SIDEBARWIDGET_HPP

#include <QWidget>

#include <QVBoxLayout>
#include <QListView>
#include <QPushButton>

#include "../interfaces/restinterface.hpp"
#include "searchwidget.hpp"

namespace Alfa
{

class SideBarWidget :
    public QWidget,
    public RestInterface
{
    Q_OBJECT
    Q_INTERFACES(Alfa::RestInterface)

public:
    explicit SideBarWidget(QWidget* parent = 0);
    ~SideBarWidget();

    void setupUI();

private:
    QVBoxLayout* rootLayout;
    QListView* accountList;

    SearchWidget* searchWidget;

    void handleSuccess(const QJsonObject&) override;
    void handleError(const QJsonObject&) override;

signals:

public slots:
};


} // namespace Alfa



#endif // SIDEBARWIDGET_HPP
