#ifndef SEARCHWIDGET_HPP
#define SEARCHWIDGET_HPP

#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>
#include <QLineEdit>
#include <QEvent>

#include "../interfaces/restinterface.hpp"
#include "../models/account.mdl.hpp"



namespace Alfa
{

class SearchWidget :
    public QWidget,
    public RestInterface
{
    Q_OBJECT
    Q_INTERFACES(Alfa::RestInterface)

public:
    explicit SearchWidget(QWidget* parent = nullptr);
    ~SearchWidget() {}

    void setupUI();

    void customEvent(QEvent* event);


private:
    QHBoxLayout* rootLayout;
    QPushButton* searchButton;
    QLineEdit* searchEdit;

signals:
    void modelChanged(AccountModel*);

public slots:
};


} // namespace Alfa



#endif // SEARCHWIDGET_HPP
