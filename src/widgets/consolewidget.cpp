#include "consolewidget.hpp"
#include <QTextEdit>


namespace Alfa
{


ConsoleWidget::ConsoleWidget(QWidget* parent) : QWidget(parent)
{
    rootLayout = new QGridLayout(this);
}

ConsoleWidget::~ConsoleWidget() {}

void ConsoleWidget::setupUI()
{

    rootLayout->setMargin(1);

    QTextEdit* stub_ = new QTextEdit(this);

    rootLayout->addWidget(stub_);
    setLayout(rootLayout);
}

} // namespace Alfa

