#include "searchwidget.hpp"

#include <QSharedDataPointer>

#include "application.hpp"

#include "interfaces/restinterface.hpp"
#include "core/rpc_client.hpp"
#include "core/helper.hpp"

#include "events/on_account.hpp"


namespace Alfa
{


SearchWidget::SearchWidget(QWidget* parent) : QWidget(parent)
{
    rootLayout = new QHBoxLayout(this);
    searchButton = new QPushButton(this);
    searchEdit = new QLineEdit(this);
}

void SearchWidget::setupUI()
{
    searchButton->setText("Push");

    rootLayout->setMargin(1);
    rootLayout->addWidget(searchEdit);
    rootLayout->addWidget(searchButton);

    setLayout(rootLayout);

    connect(searchButton, &QPushButton::pressed, [&]()
    {
        auto& aApp = Application::getApp();
        auto& rpc = aApp.getRpcClient();

        auto callBack =  [&](gpb::Message * res)
        {
            Proto::GetAccountByIdRes* response = dynamic_cast<Proto::GetAccountByIdRes*>(res);

            if (!response->has_error())

            {
                qDebug() << response->ShortDebugString().c_str();

//                auto authToken = QString(response->user_token().c_str());
//                emit authAccepted(authToken);
            }
            else
            {
//                emit authRejected("Error in RcpCall");
                qDebug() << "Error in RcpCall";
            }
        };

        rpc.getAccountById(searchEdit->text().toInt(), callBack);

//        emit modelChanged(accountModel);
    });

    alfaApp().addEventHandler(ON_ACCOUNT_EVENT, this);
}

void SearchWidget::customEvent(QEvent* event)
{
    if (event->type() == ON_ACCOUNT_EVENT)
        qDebug() << "SIDEBARWIDGET";

}

} // namespace Alfa
