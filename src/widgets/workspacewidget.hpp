#ifndef WORKSPACEWIDGET_HPP
#define WORKSPACEWIDGET_HPP

#include <QWidget>
#include <QGridLayout>

namespace Alfa
{

class WorkSpaceWidget : public QWidget
{
    Q_OBJECT
public:
    explicit WorkSpaceWidget(QWidget* parent = nullptr);
    ~WorkSpaceWidget();

    void setupUI();

private:
    QGridLayout* rootLayout;


signals:

public slots:
};


} // namespace Alfa


#endif // WORKSPACEWIDGET_HPP
