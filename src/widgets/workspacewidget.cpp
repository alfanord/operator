#include "workspacewidget.hpp"

#include <QTextEdit>

namespace Alfa
{

WorkSpaceWidget::WorkSpaceWidget(QWidget* parent) :
    QWidget(parent)
{
    rootLayout = new QGridLayout(this);
}


WorkSpaceWidget::~WorkSpaceWidget() {}

void WorkSpaceWidget::setupUI()
{
    rootLayout->setMargin(1);

    QTextEdit* stub_ = new QTextEdit(this);

    rootLayout->addWidget(stub_);
    setLayout(rootLayout);
}


} // namespace Alfa


