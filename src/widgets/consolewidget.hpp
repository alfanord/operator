#ifndef CONSOLEWIDGET_HPP
#define CONSOLEWIDGET_HPP

#include <QWidget>

#include <QGridLayout>

namespace Alfa
{

class ConsoleWidget : public QWidget
{
public:
    explicit ConsoleWidget(QWidget* parent);
    ~ConsoleWidget();

    void setupUI();

protected:


private:
    QGridLayout* rootLayout;


public slots:

};


} // namespace Alfa



#endif // CONSOLEWIDGET_HPP
