#include "mainwidget.hpp"

namespace Alfa
{

MainWidget::MainWidget(QWidget* parent) : QWidget(parent)
{
    rootLayout = new QHBoxLayout(this);
    workSpaceWidget = new WorkSpaceWidget(this);
    consoleWidget = new ConsoleWidget(this);
    rootSplitter = new QSplitter(Qt::Vertical, this);
}

MainWidget::~MainWidget() {}

void MainWidget::setupUI()
{
    workSpaceWidget->setupUI();
    consoleWidget->setupUI();

    rootLayout->setMargin(1);

    rootSplitter->addWidget(workSpaceWidget);
    rootSplitter->addWidget(consoleWidget);

    rootSplitter->setStretchFactor(0, 1);
    rootSplitter->setStretchFactor(1, 0);

    rootLayout->addWidget(rootSplitter);

    setLayout(rootLayout);
}

} // namespace Alfa

