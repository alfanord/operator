#ifndef MAINWIDGET_HPP
#define MAINWIDGET_HPP

#include <QWidget>
#include <QHBoxLayout>
#include <QSplitter>



#include "workspacewidget.hpp"
#include "consolewidget.hpp"

namespace Alfa
{

class MainWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MainWidget(QWidget* parent = nullptr);
    ~MainWidget();

    void setupUI();

private:
    QHBoxLayout* rootLayout;

    QSplitter* rootSplitter;

    WorkSpaceWidget* workSpaceWidget;
    ConsoleWidget* consoleWidget;

};


} // namespace Alfa



#endif // MAINWIDGET_HPP
