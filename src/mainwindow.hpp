#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QVBoxLayout>
#include <QSplitter>

#include "widgets/sidebarwidget.hpp"
#include "widgets/mainwidget.hpp"

#include "interfaces/restinterface.hpp"


namespace Ui
{
class MainWindow;
}


class MainWindow :
    public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow* ui;

    QVBoxLayout* rootLayout;
    QSplitter* rootSplitter;

    Alfa::SideBarWidget* sideBarWidget;
    Alfa::MainWidget* mainWidget;



public slots:
    void setupGeometry(const QString& token);

    void setupUI();

    void refreshUI();

};

#endif // MAINWINDOW_HPP
