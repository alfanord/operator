#ifndef PERSON_HPP
#define PERSON_HPP

#include <QObject>

#include "base.cl.hpp"

#include "../protocol/person.pb.h"

namespace Alfa
{

class Person :
    public QObject,
    public Base::Class,
    public Proto::Person
{
    Q_OBJECT
public:
    using QObject::QObject;

    virtual ~Person();
signals:

public slots:
};

} // namespace Alfa

#endif // PERSON_HPP
