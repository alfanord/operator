#ifndef CLASSBASE_HPP
#define CLASSBASE_HPP

#include <QVariantMap>

namespace Alfa
{

namespace Base
{

class Class
{

public:
    Class() {}
    virtual ~Class() {}

protected:
};

} // namespace Base
} // namespace Alfa

#endif // CLASSBASE_HPP
