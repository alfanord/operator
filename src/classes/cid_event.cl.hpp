#ifndef CID_EVENT_HPP
#define CID_EVENT_HPP

#include <QObject>

#include "classes/base.cl.hpp"

#include "protocol/event.pb.h"

namespace Alfa
{

class CidEvent :
    public QObject,
    public Proto::Event
{
    Q_OBJECT
public:
    explicit CidEvent(QObject* parent = nullptr);
    virtual ~CidEvent() {}

signals:

public slots:
};

} // namespace Alfa

#endif // CID_EVENT_HPP
