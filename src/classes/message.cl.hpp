#ifndef MESSAGECLASS_HPP
#define MESSAGECLASS_HPP

#include <QObject>


#include "base.cl.hpp"

#include "../protocol/message.pb.h"

namespace Alfa
{

class Message :
    public QObject,
    public Base::Class,
    public Proto::Message
{
    Q_OBJECT
public:
    using QObject::QObject;
    virtual ~Message();

signals:

public slots:
};

} // namespace Alfa

#endif // MESSAGECLASS_HPP
