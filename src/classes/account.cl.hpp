#ifndef ACCOUNT_HPP
#define ACCOUNT_HPP

#include <QObject>

#include "base.cl.hpp"

#include "../protocol/account.pb.h"

namespace Alfa
{

class Account :
    public QObject,
    public Base::Class,
    public Proto::Account

{
    Q_OBJECT

public:
    using QObject::QObject;
    virtual ~Account();

signals:

private:

public slots:
};

} // namespace Alfa

#endif // ACCOUNT_HPP

