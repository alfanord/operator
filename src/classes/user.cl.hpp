#ifndef USER_HPP
#define USER_HPP

#include <QObject>

#include "base.cl.hpp"

#include "protocol/user.pb.h"

namespace Alfa
{

class User :
    public QObject,
    public Base::Class,
    public Proto::User
{
    Q_OBJECT
public:
    explicit User(QObject* parent = nullptr);

    virtual ~User() {}

signals:

public slots:
};

} // namespace Alfa

#endif // USER_HPP
