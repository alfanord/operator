#ifndef CID_EVENTTYPE_CL_HPP
#define CID_EVENTTYPE_CL_HPP

#include <QObject>

#include "classes/base.cl.hpp"

#include "protocol/event_type.pb.h"

namespace Alfa
{

class CidEventType :
    public QObject,
    public Base::Class,
    public Proto::EventType
{
    Q_OBJECT
public:
    explicit CidEventType(QObject* parent = nullptr);
    virtual ~CidEventType() {}

signals:

public slots:
};

} // namespace Alfa

#endif // CID_EVENTTYPE_CL_HPP
