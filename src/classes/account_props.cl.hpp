#ifndef ACCOUNT_PROPS_CL_HPP
#define ACCOUNT_PROPS_CL_HPP

#include <QObject>

#include "classes/base.cl.hpp"

#include "protocol/account_props.pb.h"

namespace Alfa
{

class AccountFamily : public QObject, public Base::Class, public Proto::AccountFamily
{
    Q_OBJECT
public:
    explicit AccountFamily(QObject* parent = nullptr);
    virtual ~AccountFamily() {}

signals:

public slots:
};

class AccountImportance : public QObject, public Base::Class, public Proto::AccountImportance
{
    Q_OBJECT
public:
    AccountImportance() {}
    virtual ~AccountImportance() {}

};

class AccountCategory : public QObject, public Base::Class, public Proto::AccountCategory
{
    Q_OBJECT
public:
    AccountCategory() {}
    virtual ~AccountCategory() {}
};

class AccountService : public QObject, public Base::Class, public Proto::AccountService
{
    Q_OBJECT
public:
    AccountService() {}
    virtual ~AccountService() {}
};


class AccountEquipment : public QObject, public Base::Class, public Proto::AccountEquipment
{
    Q_OBJECT
public:
    AccountEquipment() {}
    virtual ~AccountEquipment() {}
};

class AccountReaction : public QObject, public Base::Class, public Proto::AccountReaction
{
    Q_OBJECT
public:
    AccountReaction() {}
    virtual ~AccountReaction() {}
};

class AccountRegion : public QObject, public Base::Class, public Proto::AccountRegion
{
    Q_OBJECT
public:
    AccountRegion() {}
    virtual ~AccountRegion() {}
};


} // namespace Alfa

#endif // ACCOUNT_PROPS_CL_HPP
