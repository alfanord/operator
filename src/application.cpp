#include "application.hpp"

#include <QObject>
#include <QTimer>

#include "protocol/auth.pb.h"

#include "events/on_account.hpp"

namespace Alfa
{

Application::~Application()
{
    delete settings;
    delete restClient;
    delete rpcClient;
    delete rpcChannel;
}

Application::Application(int& argc, char** argv):
    QApplication(argc, argv)
{
    setOrganizationName("AlfaNord");
    setApplicationName("OperatorUI");
    setApplicationVersion("0.0.0");

    settings = new Settings(this);

    restClient = new RestClient(*settings, this);
    rpcChannel = new RpcChannel(*settings, this);

    rpcClient = new RpcClient(rpcChannel, this);

    QTimer::singleShot(0, this, &Application::start);
}


void Application::start()
{
    auto lastRun = settings->read("General", "last_run", "first run").toString();
    qDebug() << "последний запуск:" << lastRun;

    auto now = QDateTime::currentDateTime();
    settings->save("General", "last_run", now.toString(Qt::TextDate));

    emit started();
}

void Application::initAccounts()
{
    auto callBack = [&](gpb::Message * msg)
    {
        Proto::GetAccountsRes* response_ = dynamic_cast<Proto::GetAccountsRes*>(msg);


        for (int i = 0; i < response_->accounts_size(); ++i)
        {

            Account* acc = new Account;

            acc->CopyFrom(response_->accounts(i));

            qDebug() << response_->accounts_size();

            for (auto const& handler : eventHandlers[ON_ACCOUNT_EVENT])
                postEvent(handler, new OnAccount(acc));

//            close();

//            for (auto const& i : auth_msg->user().roles())
//                qDebug() << i.c_str();

//            for (int i = 0; i < auth_msg->user().roles_size(); ++i)
//                qDebug() << auth_msg->user().roles(i).c_str();

//            auto authToken = QString(auth_msg->user_token().c_str());
//            emit authAccepted(authToken);
        }


    };

    rpcClient->getAccounts(callBack);

}

Settings& Application::getSettings() const
{
    return *settings;
}

RestClient& Application::getRestClient() const
{
    return *restClient;
}

RpcClient& Application::getRpcClient() const
{
    return *rpcClient;
}

RpcChannel& Application::getRpcChannel() const
{
    return *rpcChannel;
}

Application::ptrUser Application::getUser() const
{
    return user;
}

void Application::setUser(Application::ptrUser user_)
{
    user = user_;
}




Application& Application::getApp()
{
    return *qobject_cast<Application*>(qApp);
}

} // namespace Alfa

