#ifndef ON_ACCOUNT_HPP
#define ON_ACCOUNT_HPP

#include <QEvent>
#include <QSharedPointer>

#include "classes/account.cl.hpp"

namespace Alfa
{

const QEvent::Type ON_ACCOUNT_EVENT = static_cast<QEvent::Type>(QEvent::User + 1);

class OnAccount:
    public QEvent
{
public:
    OnAccount(Account* account_):
        QEvent(ON_ACCOUNT_EVENT),
        account(account_)
    {
    }

    virtual ~OnAccount()
    {
    }

    Account* getData() const
    {
        return account.data();
    }

private:
    QSharedPointer<Account> account;
};

} // namespace Alfa

#endif // ON_ACCOUNT_HPP
