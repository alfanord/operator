#ifndef LOGINDIALOG_HPP
#define LOGINDIALOG_HPP


#include <QSettings>
#include <QFormLayout>
#include <QMessageBox>

#include "../core/settings.hpp"
#include "../interfaces/restinterface.hpp"

namespace Ui
{
class LoginDialog;
}

class LoginDialog :
    public QDialog
{
    Q_OBJECT
public:
    explicit LoginDialog(QWidget* parent = 0);
    ~LoginDialog();

//    void setupUI();
//    void showUI(const QString&);

private:
    Ui::LoginDialog* ui;

//    void handleSuccess(const QJsonObject&) override;
//    void handleError(const QJsonObject&) override;



signals:
    void authAccepted(const QString&);
    void authRejected(const QString&);
    void requestSent();

public slots:
    void accept() override;
    void checkAuth();


};

#endif // LOGINDIALOG_HPP
