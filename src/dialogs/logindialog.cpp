#include "logindialog.hpp"
#include "ui_logindialog.h"

#include <QDebug>

#include "../core/settings.hpp"

#include "../core/helper.hpp"

#include "protocol/auth.pb.h"

#include <iostream>

namespace gpb = google::protobuf;


LoginDialog::LoginDialog(QWidget* parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);
    ui->userEdit->setText("");
    ui->passEdit->setText("");
}

LoginDialog::~LoginDialog()
{
    delete ui;
}


void LoginDialog::accept()
{
    auto userName = ui->userEdit->text();
    auto userPass = ui->passEdit->text();

    auto& app = alfaApp().getApp();

    auto callBack = [&](gpb::Message * msg)
    {
        Proto::AuthLoginRes* response = dynamic_cast<Proto::AuthLoginRes*>(msg);

        if (!response->has_error())

        {
            qDebug() << response->ShortDebugString().c_str();

            Alfa::Application::ptrUser user = QSharedPointer<Alfa::User>(new Alfa::User);
            user->CopyFrom(response->user());
            alfaApp().getApp().setUser(user);

            for (auto const& i : response->user().roles())
                qDebug() << i.c_str();

            for (int i = 0; i < response->user().roles_size(); ++i)
                qDebug() << response->user().roles(i).c_str();

            auto authToken = QString(response->user_token().c_str());
            emit authAccepted(authToken);
            close();
        }
        else
        {
            emit authRejected("Error in RcpCall");
            qDebug() << "Error in RcpCall";
        }


    };

    app.getRpcClient().authLogin(userName, userPass, callBack);

}

/*
void LoginDialog::handleSuccess(const QJsonObject& result)
{
    qDebug()  << result;

    if (result.contains("user_token") && result["user_token"].isString())
    {
        close();

        auto authToken = result["user_token"].toString();
        emit authAccepted(authToken);
    }
}

void LoginDialog::handleError(const QJsonObject& result)
{
    QString error;

    if (result.contains("QNetworkReplyError"))
    {
        qWarning()  << result;
        error = result["QNetworkReplyError"].toString();
    }

    emit authRejected(error);
}
*/

void LoginDialog::checkAuth()
{

    auto& app = alfaApp().getApp();

    const QString userToken = app.getSettings().read("General", "auth_token", "").toString();

    auto callBack = [&](gpb::Message * msg)
    {
        Proto::AuthRefreshRes* response = dynamic_cast<Proto::AuthRefreshRes*>(msg);

        if (!response->has_error())

        {
            qDebug() << response->ShortDebugString().c_str();

            Alfa::Application::ptrUser user = QSharedPointer<Alfa::User>(new Alfa::User);
            user->CopyFrom(response->user());
            alfaApp().getApp().setUser(user);

            auto authToken = QString(response->user_token().c_str());
            emit authAccepted(authToken);

            close();
        }
        else
        {
            emit authRejected("Error in RcpCall");
            qDebug() << "Error in RcpCall";
        }


    };

    app.getRpcClient().authRefresh(userToken, callBack);
}
