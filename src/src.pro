#-------------------------------------------------
#
# Project created by QtCreator 2018-02-10T11:28:47
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = alfanord
TEMPLATE = app

CONFIG += c++1z

LIBS += -lprotobuf

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

QMAKE_CXXFLAGS_WARN_ON = -Wall -Wno-unused-parameter

SOURCES += \
    main.cpp \
    runguard.cpp \
    application.cpp \
    mainwindow.cpp \
    core/settings.cpp \
    core/restclient.cpp \
    interfaces/restinterface.cpp \
    widgets/mainwidget.cpp \
    widgets/sidebarwidget.cpp \
    widgets/workspacewidget.cpp \
    widgets/consolewidget.cpp \
    widgets/searchwidget.cpp \
    dialogs/logindialog.cpp \
    protocol/account_props.pb.cc \
    protocol/account.pb.cc \
    protocol/auth.pb.cc \
    protocol/channel.pb.cc \
    protocol/common.pb.cc \
    protocol/csm.pb.cc \
    protocol/event_type.pb.cc \
    protocol/event.pb.cc \
    protocol/message.pb.cc \
    protocol/packet.pb.cc \
    protocol/packet_rpc.pb.cc \
    protocol/service_alfa.pb.cc \
    protocol/person.pb.cc \
    protocol/surgard.pb.cc \
    protocol/trouble.pb.cc \
    protocol/user.pb.cc \
    protocol/wialon_unit.pb.cc \
    protocol/ips.pb.cc \
    core/helper.cpp \
    classes/account.cl.cpp \
    classes/base.cl.cpp \
    classes/message.cl.cpp \
    classes/person.cl.cpp \
    classes/user.cl.cpp \
    models/account.mdl.cpp \
    models/base.mdl.cpp \
    core/rpc_controller.cpp \
    core/rpc_channel.cpp \
    core/rpc_client.cpp \
    core/rpc_result.cpp \
    core/rpc_call.cpp \
    classes/cid_eventtype.cl.cpp \
    classes/account_props.cl.cpp \
    classes/cid_event.cl.cpp \
    events/on_account.cpp

HEADERS += \
    runguard.hpp \
    application.hpp \
    mainwindow.hpp \
    core/restclient.hpp \
    core/settings.hpp \
    interfaces/restinterface.hpp \
    widgets/mainwidget.hpp \
    widgets/sidebarwidget.hpp \
    widgets/workspacewidget.hpp \
    widgets/consolewidget.hpp \
    widgets/searchwidget.hpp \
    dialogs/logindialog.hpp \
    protocol/account_props.pb.h \
    protocol/account.pb.h \
    protocol/auth.pb.h \
    protocol/channel.pb.h \
    protocol/common.pb.h \
    protocol/csm.pb.h \
    protocol/event_type.pb.h \
    protocol/event.pb.h \
    protocol/message.pb.h \
    protocol/packet.pb.h \
    protocol/packet_rpc.pb.h \
    protocol/service_alfa.pb.h \
    protocol/person.pb.h \
    protocol/surgard.pb.h \
    protocol/trouble.pb.h \
    protocol/user.pb.h \
    protocol/wialon_unit.pb.h \
    protocol/ips.pb.h \
    core/helper.hpp \
    classes/account.cl.hpp \
    classes/base.cl.hpp \
    classes/message.cl.hpp \
    classes/person.cl.hpp \
    classes/user.cl.hpp \
    models/account.mdl.hpp \
    models/base.mdl.hpp \
    core/rpc_controller.hpp \
    core/rpc_client.hpp \
    core/rpc_channel.hpp \
    core/rpc_result.hpp \
    core/rpc_call.hpp \
    classes/cid_eventtype.cl.hpp \
    classes/account_props.cl.hpp \
    classes/cid_event.cl.hpp \
    events/on_account.hpp

FORMS += \
    mainwindow.ui \
    dialogs/logindialog.ui

INCLUDEPATH += \
    src
