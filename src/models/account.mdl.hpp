#ifndef ACCOUNTMODEL_HPP
#define ACCOUNTMODEL_HPP

#include <QAbstractTableModel>
#include <QMap>

#include "base.mdl.hpp"

//#include "../interfaces/restinterface.hpp"

namespace Alfa
{

class AccountModel :
    public QAbstractListModel,
    public ModelBase
{
    Q_OBJECT
public:
    using QAbstractListModel::QAbstractListModel;
    ~AccountModel();

    /* FIXME: implement setup model header
    // Header:
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    bool setHeaderData(int section, Qt::Orientation orientation, const QVariant& value, int role = Qt::EditRole) override;
    */

    // Basic functionality:
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;

    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

    // Editable:
//    bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;

    /* FIXME: implement virtual methods

    // Add data:
    bool insertRows(int row, int count, const QModelIndex& parent = QModelIndex()) override;
    bool insertColumns(int column, int count, const QModelIndex& parent = QModelIndex()) override;

    // Remove data:
    bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex()) override;
    bool removeColumns(int column, int count, const QModelIndex& parent = QModelIndex()) override;
    */

    virtual void setupUI() {}

private:
    QVector<QSharedPointer<>> modelData;
signals:
    void testSingal();

};

} // namespace Alfa



#endif // ACCOUNTMODEL_HPP
