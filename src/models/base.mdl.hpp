#ifndef MODELBASE_HPP
#define MODELBASE_HPP

#include <QObject>
#include <QMap>

#include <QSharedPointer>

#include <QDebug>

namespace Alfa
{

class ModelBase
{
public:
    ModelBase() {}
    virtual ~ModelBase() {}

    template <class AlfaModel>
    void init(const QMap<int, AlfaModel*>& modelMap)
    {
        modelData.clear();

        for (auto const& idx : modelMap.keys())
        {
            auto alfaObject = modelMap[idx];

            modelData[idx] = qobject_cast<QObject*>(alfaObject);
        }
    }

protected:
    QMap<int, QObject*> modelData;

private:


};

} // namespace Alfa

#endif // MODELBASE_HPP
