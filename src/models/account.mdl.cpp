#include "account.mdl.hpp"

#include "../classes/account.cl.hpp"

namespace Alfa
{

AccountModel::~AccountModel() {}

/* FIXME: implement virtual methods
QVariant AccountModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    // FIXME: Implement me!
}

bool AccountModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant& value, int role)
{
    if (value != headerData(section, orientation, role))
    {
        // FIXME: Implement me!
        emit headerDataChanged(orientation, section, section);
        return true;
    }

    return false;
}
*/

int AccountModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid())
        return 0;

    return modelData.size();
}

int AccountModel::columnCount(const QModelIndex& parent) const
{
    if (parent.isValid())
        return 0;

    return 4;
}

QVariant AccountModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    auto accountModel = modelData.value(index.row());

    if (nullptr != accountModel)
    {
        Account* account = qobject_cast<Account*>(accountModel);

        if (role == Qt::DisplayRole)
            return QVariant(account->title().c_str());
    }

    return QVariant();
}

Qt::ItemFlags AccountModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable; // FIXME: Implement me!
}


/* FIXME: implement virtual methods

bool AccountModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (data(index, role) != value)
    {
        // FIXME: Implement me!

        qDebug() << index << value.toString();
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }

    return false;
}



bool AccountModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endInsertRows();
}

bool AccountModel::insertColumns(int column, int count, const QModelIndex &parent)
{
    beginInsertColumns(parent, column, column + count - 1);
    // FIXME: Implement me!
    endInsertColumns();
}

bool AccountModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(parent, row, row + count - 1);
    // FIXME: Implement me!
    endRemoveRows();
}

bool AccountModel::removeColumns(int column, int count, const QModelIndex &parent)
{
    beginRemoveColumns(parent, column, column + count - 1);
    // FIXME: Implement me!
    endRemoveColumns();
}
*/


} // namespace Alfa
