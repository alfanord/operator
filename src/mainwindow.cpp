#include "mainwindow.hpp"
#include "ui_mainwindow.h"

#include <QDebug>

#include <QDesktopWidget>
#include <QStyle>
#include <QTimer>


MainWindow::MainWindow(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    rootLayout = new QVBoxLayout(ui->centralWidget);
    rootSplitter = new QSplitter(ui->centralWidget);
    sideBarWidget = new Alfa::SideBarWidget(ui->centralWidget);
    mainWidget = new Alfa::MainWidget(ui->centralWidget);
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::setupUI()
{

    sideBarWidget->setupUI();
    mainWidget->setupUI();

    rootSplitter->addWidget(sideBarWidget);
    rootSplitter->addWidget(mainWidget);

    rootSplitter->setStretchFactor(0, 0);
    rootSplitter->setStretchFactor(1, 1);

    rootLayout->addWidget(rootSplitter);

    ui->centralWidget->setLayout(rootLayout);

}

void MainWindow::setupGeometry(const QString& authToken)
{
    qDebug() << authToken;

    QRect availableGeometry = qApp->desktop()->availableGeometry();
    QSize availableSize = availableGeometry.size();

    int screenWidth = availableSize.width();
    int screenHeight = availableSize.height();

    QSize newSize(screenWidth * 0.8, screenHeight * 0.8);
    QRect newGeometry = QStyle::alignedRect(Qt::LeftToRight, Qt::AlignCenter, newSize, availableGeometry);

    setGeometry(newGeometry);
}

void MainWindow::refreshUI()
{
    qDebug() << this << "mainWindow resfreshUI slot" ;
}
