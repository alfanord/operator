# AlfaNord Operator UI

[![pipeline status](https://gitlab.com/alfanord/operator/badges/master/pipeline.svg)](https://gitlab.com/alfanord/operator/commits/master)
[![coverage report](https://gitlab.com/alfanord/operator/badges/master/coverage.svg)](https://alfanord.gitlab.io/operator)
[![codecov](https://codecov.io/gl/alfanord/operator/branch/master/graph/badge.svg)](https://codecov.io/gl/alfanord/operator)
