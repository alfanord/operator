FROM fedora:27
LABEL maintainer="roman@stolyarch.uk"
# RUN dnf install -y gcc-c++ qt5-qtbase-devel qt5-qtdeclarative-devel qt5-qtlocation-devel qt5-qtscript-devel zeromq-devel gcovr findutils
RUN dnf install -y gcc-c++ qt5-qtbase-devel zeromq-devel gcovr findutils
